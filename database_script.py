from sqlalchemy import Column, Integer, String
from sqlalchemy import CheckConstraint, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    email = Column(String(50), unique=True)
    age = Column(Integer)
    username = Column(String(50), nullable=False, unique=True)
    password = Column(String(50), nullable=False)
    confirmed = Column(Integer, nullable=False)
    __table_arg__ = (
        CheckConstraint(age > 0))


engine = create_engine(
    'sqlite:///database.db', connect_args={'check_same_thread': False})
Base.metadata.create_all(engine)

BDSession = sessionmaker(bind=engine)
session = BDSession()


def insert_user(name, email, age, user, psw, confirmed):
    new_user = User(
        name=name, email=email, age=age,
        username=user, password=psw, confirmed=confirmed)
    session.add(new_user)
    session.commit()


def confirm_user_bd(email):
    user = session.query(User).filter(User.email == email).first()
    user.confirmed = 1
    session.commit()


def get_username_from_db(email):
    user = session.query(User).filter(User.email == email).first()
    return user.username


def get_user(username):
    user = session.query(User).filter(
        User.username == username, User.confirmed == 1).first()
    return user


def usernames_emails():
    users = session.query(User.username, User.email)
    return users


if __name__ == '__main__':
    users = usernames_emails()
    print(users[1])
