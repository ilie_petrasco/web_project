import os
import datetime
from waitress import serve
from flask import Flask, render_template, request, session, redirect, url_for
from argon2 import PasswordHasher
from validate_email import validate_email
from flask_mail import Mail, Message
from itsdangerous import URLSafeTimedSerializer, SignatureExpired
from itsdangerous import BadTimeSignature
from database_script import insert_user, confirm_user_bd, get_username_from_db
from database_script import get_user, usernames_emails


app = Flask(__name__)
app.debug = True
app.config['MAIL_PASSWORD'] = os.environ['SMTP_PASSWORD']
app.config.from_pyfile('config.cfg')
app.secret_key = 'super secret key'

mail = Mail(app)
s = URLSafeTimedSerializer(app.config['SECRET_KEY'])


@app.route('/', methods=['GET', 'POST'])
def index():
    if session.get('loged_in') is True:
        return render_template('index.html', user=session.get('username'))
    else:
        return render_template('login.html')


@app.route('/logout', methods=['GET', 'POST'])
def logout():
    session.pop('username', None)
    session.pop('loged_in', None)
    return redirect(url_for('index'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    username = request.form['username']
    password = request.form['password']
    ph = PasswordHasher()
    user = get_user(username)
    # the user exists in database and it's confirmed
    if user:
        try:
            if ph.verify(user.password, password) is True:
                session['username'] = username
                session['loged_in'] = True
                return redirect(url_for('index'))
        except:
            pass
    session['login_error'] = True
    return redirect(url_for('index'))


@app.route('/registration', methods=['GET', 'POST'])
def registration():

    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        age = request.form['age']
        username = request.form['username']
        password = request.form['password']

        register_error = ''
        users = usernames_emails()
        exit = 0
        for user in users:
            if username in user:
                register_error += '*username already exists '
                exit += 1
            if email in user:
                register_error += '*email address already used '
                exit += 1
            # no need to iterate further
            # once email and username alredy taken
            if exit == 2:
                break

        if len(password) < 6:
            register_error += '*password field'
            'must be longer than 6 characters'
        if len(username) < 6:
            register_error += '*username field'
            'must be longer than 6 characters'
        try:
            if int(age) < 0 and int(age) > 120:
                register_error += '*age field'
                'can not be negative or greater than 120'
        except ValueError:
                register_error += '*age field'
                'can not be negative or greater than 120'
        if len(name) < 1:
            register_error += '*name field can not be empty'
        if not validate_email(email):
            register_error += '*wrong email format'

        if not register_error:
            token = s.dumps(email, salt='my_salt')
            msg = Message(
                'Confirm Email address', sender='iliepetrasco@gmail.com',
                recipients=[email])
            link = url_for('confirm_email', token=token, _external=True)
            msg.body = 'Your confirmation link is {}'.format(link)
            mail.send(msg)

            ph = PasswordHasher()
            hash = ph.hash(password)
            insert_user(name, email, age, username, hash, 0)
            return '<h1>A confirmation email has been send on this address:{}. Please confirm your registration.</h1>'.format(email)
        else:
            return render_template(
                'registration.html', register_error=register_error)

    return render_template('registration.html')


@app.route('/confirm_email/<token>')
def confirm_email(token):
    try:
        email = s.loads(token, salt='my_salt', max_age=3600)
    except SignatureExpired:
        return "<h1>The confirmation link expired!</h1>"
    except BadTimeSignature:
        return "<h1>It has been used an invalid confirmation link!</h1>"
    confirm_user_bd(email)
    # create a folder for new user
    username = get_username_from_db(email)
    if not os.path.exists('user_files/' + username):
        os.makedirs('user_files/' + username)
    return '<h1>Succefull confirmation!!!   </h1>'


@app.route('/view_files')
def view_files():
    if session.get('loged_in') is True:
        # build the path to user's folder
        path = 'user_files/' + session.get('username')
        # check is the user's folder exists
        if os.path.isdir(path):
            dirs = os.listdir(path)
            # build table header
            table = '''<table class='table table-hover'>
                        <tr>
                        <th>File name</th>
                        <th>Date modified</th>
                        <th>View</th>
                        <th>Delete</th>
                        </tr>
            '''

            for file in dirs:
                full_path = path + '/' + file
                if os.path.isfile(full_path):
                    time_modified = datetime.datetime.fromtimestamp(
                        os.path.getmtime(full_path)).strftime(
                        '%Y-%m-%d  %H:%M:%S')
                    view_button = '''<button class="view_button" onclick="viewFile(\'{}\')">
                                <span class="glyphicon">&#xe003;</span></button>'''.format(full_path)
                    delete_button = '''<button class="view_button" onclick="deleteFile(\'{}\')">
                                <span class="glyphicon">&#xe014;</span></button>'''.format(full_path)
                    row = '''<tr><td>{}</td><td>{}</td><td>{}</td><td>{}</td>
                            '''.format(file, time_modified, view_button, delete_button)
                    table += row
            return table
        return '<h1>Server error! Your files have been corrupted!</h1>'
    else:
        return '<h1> Page is not available!</h1>'


@app.route('/save_file')
def save_file():
    if session.get('loged_in') is True:
        file_name = request.values['file_name']
        GO = True
        restricted = ['/', '\\', ':', '*', '\"', '<', '>', '|', '\'']
        for c in file_name:
            if c in restricted:
                GO = False
                break
        if GO is False:
            return "*wrong filename"
        else:
            textContent = request.values['textContent']
            file_path = 'user_files/' + session.get('username') + '/' + file_name

            if os.path.isfile(file_path):
                status = "*File modified!"
            else:
                status = "*File created!"
            with open(file_path, mode='w+') as f:
                f.writelines(textContent)
            return status


@app.route('/view_file')
def view_file():
    file_name = request.values['file_name']
    content = 'start :  '
    with open(file_name) as f:
        content = f.read()
    return content


@app.route('/delete_file', methods=['POST'])
def delete_file():
    if session.get('loged_in') is True:
        file_name = request.values['file_name']
        os.remove(file_name)
        return "*File deleted!"


def create_users_folder():
    if not os.path.exists('user_files'):
        os.makedirs('user_files')


if __name__ == '__main__':
    create_users_folder()
    serve(app, host='localhost', port=8080)
    # app.run('localhost', 8080)
